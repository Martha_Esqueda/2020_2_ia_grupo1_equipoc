﻿namespace EjercicioPOO
{
    partial class FDatosMateria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNombreMateria = new System.Windows.Forms.TextBox();
            this.txtClaveMateria = new System.Windows.Forms.TextBox();
            this.nUDCreditos = new System.Windows.Forms.NumericUpDown();
            this.btnCrearMateria = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nUDCreditos)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre Materia:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Clave Materia:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(75, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Creditos:";
            // 
            // txtNombreMateria
            // 
            this.txtNombreMateria.Location = new System.Drawing.Point(151, 36);
            this.txtNombreMateria.Name = "txtNombreMateria";
            this.txtNombreMateria.Size = new System.Drawing.Size(208, 22);
            this.txtNombreMateria.TabIndex = 3;
            // 
            // txtClaveMateria
            // 
            this.txtClaveMateria.Location = new System.Drawing.Point(151, 76);
            this.txtClaveMateria.Name = "txtClaveMateria";
            this.txtClaveMateria.Size = new System.Drawing.Size(208, 22);
            this.txtClaveMateria.TabIndex = 4;
            // 
            // nUDCreditos
            // 
            this.nUDCreditos.Location = new System.Drawing.Point(151, 116);
            this.nUDCreditos.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.nUDCreditos.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nUDCreditos.Name = "nUDCreditos";
            this.nUDCreditos.Size = new System.Drawing.Size(120, 22);
            this.nUDCreditos.TabIndex = 5;
            this.nUDCreditos.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // btnCrearMateria
            // 
            this.btnCrearMateria.Location = new System.Drawing.Point(272, 163);
            this.btnCrearMateria.Name = "btnCrearMateria";
            this.btnCrearMateria.Size = new System.Drawing.Size(131, 31);
            this.btnCrearMateria.TabIndex = 6;
            this.btnCrearMateria.Text = "Aceptar";
            this.btnCrearMateria.UseVisualStyleBackColor = true;
            this.btnCrearMateria.Click += new System.EventHandler(this.btnCrearMateria_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(123, 163);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(131, 31);
            this.btnCancelar.TabIndex = 7;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // FDatosMateria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 226);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnCrearMateria);
            this.Controls.Add(this.nUDCreditos);
            this.Controls.Add(this.txtClaveMateria);
            this.Controls.Add(this.txtNombreMateria);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MinimumSize = new System.Drawing.Size(466, 273);
            this.Name = "FDatosMateria";
            this.Text = "Datos de la Materia";
            this.Load += new System.EventHandler(this.FDatosMateria_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nUDCreditos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNombreMateria;
        private System.Windows.Forms.TextBox txtClaveMateria;
        private System.Windows.Forms.NumericUpDown nUDCreditos;
        private System.Windows.Forms.Button btnCrearMateria;
        private System.Windows.Forms.Button btnCancelar;
    }
}