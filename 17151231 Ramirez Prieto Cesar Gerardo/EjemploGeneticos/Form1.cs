﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjemploGeneticos
{
    public partial class Form1 : Form
    {
        Poblacion LaPoblacion;
        public Form1()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            LaPoblacion = new Poblacion((int)NUDTamPoblacion.Value, new EvaluadorTipo1("12345678"));

            RTBPoblacion.Text = LaPoblacion.GetDatos();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LaPoblacion.SelecionSimple((int)NUDPresion.Value);

            RTBSeleccion.Text = LaPoblacion.GetDatosSeleccionados();
        }
    }
}
