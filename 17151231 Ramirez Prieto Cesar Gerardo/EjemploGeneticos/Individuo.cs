﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploGeneticos
{
    class Individuo
    {
        string Genotipo;
            int Fitness;

        Evaluador ElEvaluador;

        public Individuo(Evaluador ElEvaluador, Random GeneradorAleatorios)
        {
            this.ElEvaluador = ElEvaluador;
            Genotipo = "";
            Fitness = 0;

            for (int i = 0; i < ElEvaluador.GetModelo().Length; i++)
                Genotipo += GeneradorAleatorios.Next(0, 9);

            Fitness = ElEvaluador.Evaluar(Genotipo);
        }

        public string GetDatos()
        {
            return Genotipo + " = " + Fitness;
        }

        public int GetFitness()
        {
            return Fitness;
        }
    }
}
