﻿namespace EjemploGeneticos
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button = new System.Windows.Forms.Button();
            this.RTBPoblacion = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.NUDTamPoblacion = new System.Windows.Forms.NumericUpDown();
            this.RTBSeleccion = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.NUDPresion = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NUDTamPoblacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDPresion)).BeginInit();
            this.SuspendLayout();
            // 
            // button
            // 
            this.button.Location = new System.Drawing.Point(198, 25);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(66, 20);
            this.button.TabIndex = 0;
            this.button.Text = "Inicializar";
            this.button.UseVisualStyleBackColor = true;
            this.button.Click += new System.EventHandler(this.button_Click);
            // 
            // RTBPoblacion
            // 
            this.RTBPoblacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.RTBPoblacion.Location = new System.Drawing.Point(18, 56);
            this.RTBPoblacion.Name = "RTBPoblacion";
            this.RTBPoblacion.ReadOnly = true;
            this.RTBPoblacion.Size = new System.Drawing.Size(246, 301);
            this.RTBPoblacion.TabIndex = 1;
            this.RTBPoblacion.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tamaño Población";
            // 
            // NUDTamPoblacion
            // 
            this.NUDTamPoblacion.Location = new System.Drawing.Point(117, 25);
            this.NUDTamPoblacion.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NUDTamPoblacion.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.NUDTamPoblacion.Name = "NUDTamPoblacion";
            this.NUDTamPoblacion.Size = new System.Drawing.Size(50, 20);
            this.NUDTamPoblacion.TabIndex = 3;
            this.NUDTamPoblacion.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // RTBSeleccion
            // 
            this.RTBSeleccion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.RTBSeleccion.Location = new System.Drawing.Point(270, 56);
            this.RTBSeleccion.Name = "RTBSeleccion";
            this.RTBSeleccion.ReadOnly = true;
            this.RTBSeleccion.Size = new System.Drawing.Size(246, 301);
            this.RTBSeleccion.TabIndex = 4;
            this.RTBSeleccion.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(270, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Presion";
            // 
            // NUDPresion
            // 
            this.NUDPresion.Location = new System.Drawing.Point(318, 27);
            this.NUDPresion.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NUDPresion.Minimum = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.NUDPresion.Name = "NUDPresion";
            this.NUDPresion.Size = new System.Drawing.Size(50, 20);
            this.NUDPresion.TabIndex = 6;
            this.NUDPresion.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(441, 27);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(66, 20);
            this.button1.TabIndex = 7;
            this.button1.Text = "Seleccionar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.NUDPresion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.RTBSeleccion);
            this.Controls.Add(this.NUDTamPoblacion);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RTBPoblacion);
            this.Controls.Add(this.button);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.NUDTamPoblacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDPresion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button;
        private System.Windows.Forms.RichTextBox RTBPoblacion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown NUDTamPoblacion;
        private System.Windows.Forms.RichTextBox RTBSeleccion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown NUDPresion;
        private System.Windows.Forms.Button button1;
    }
}