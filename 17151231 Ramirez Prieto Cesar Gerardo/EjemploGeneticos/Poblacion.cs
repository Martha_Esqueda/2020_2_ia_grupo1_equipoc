﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploGeneticos
{
    class Poblacion
    {
        List<Individuo> Individuos;
        List<Individuo> IndividuosSeleccionados;

        public Poblacion(int TamPoblacion, Evaluador ElEvaluador)
        {
            Individuos = new List<Individuo>();
            Random GeneradorAleatorios = new Random();

            for (int i = 0; i < TamPoblacion; i++)
                Individuos.Add(new Individuo(ElEvaluador, GeneradorAleatorios));

            OrdenarPorFitness();
        }

        public string GetDatos()
        {
            StringBuilder datos = new StringBuilder();

            for (int i = 0; i < Individuos.Count; i++)
                datos.Append(Individuos[i].GetDatos() + "\n");

                    return datos.ToString();
        }

        public string GetDatosSeleccionados()
        {
            StringBuilder datos = new StringBuilder();

            for (int i = 0; i < IndividuosSeleccionados.Count; i++)
                datos.Append(IndividuosSeleccionados[i].GetDatos() + "\n");

            return datos.ToString();
        }

        public void SelecionSimple(int Presion)
        {
            IndividuosSeleccionados = new List<Individuo>();

            int cantidad_seleccionados = (int)Math.Round( (100.00 - Presion) * Individuos.Count / 100);

            for (int i = 0; i < cantidad_seleccionados; i++)
                IndividuosSeleccionados.Add(Individuos[i]);
        }

        public void OrdenarPorFitness()
        {
            Individuo individuo_auxiliar = null;

            for (int i=0; i < Individuos.Count - 1; i++)
                for (int j = i + 1; j < Individuos.Count; j++)
                    if (Individuos[j].GetFitness() > Individuos[i].GetFitness())
                    {
                        individuo_auxiliar = Individuos[i];
                        Individuos[i] = Individuos[j];
                        Individuos[j] = individuo_auxiliar;
                    }
        }
    }
}
