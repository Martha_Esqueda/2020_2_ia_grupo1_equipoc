﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace KMedias
{    
    class LectorDatos
    {
        StreamReader Archivo;
        StringBuilder TextoLeido;
        string LineaActual;
        string PathFuente;
        Boolean nombreCampos=true;
       
        int numRegistro = 0;
        Cluster TodoslosRegistros = new Cluster();
        public List<Registro> Registros = new List<Registro>();

        public LectorDatos(string PathFuente ="")
        {
            if (PathFuente.Length > 0)
            {
                LeerArchivo(PathFuente);
            }
        }

        public void LeerArchivo(string PathFuente)
        {
            this.PathFuente = PathFuente;
            Archivo = new StreamReader(PathFuente);
            TextoLeido = new StringBuilder();

            LineaActual = Archivo.ReadLine();
            while (LineaActual != null)
            {
                TextoLeido.Append(LineaActual);
                if (!nombreCampos)
                {
                    Registro RegistroActual = new Registro(LineaActual);
                    //TodoslosRegistros.agregarRegistro(RegistroActual);
                    Registros.Insert(numRegistro, RegistroActual);
                    numRegistro++;
                }
                nombreCampos=false;
                TextoLeido.Append("\n");
                LineaActual = Archivo.ReadLine();
            }
        }

        public string GetTextoLeido()
        {
            return TextoLeido.ToString();
        }
    }
}
