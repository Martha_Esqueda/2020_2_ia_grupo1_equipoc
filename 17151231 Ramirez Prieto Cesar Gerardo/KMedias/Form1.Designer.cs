﻿namespace KMedias
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OFDTexto = new System.Windows.Forms.OpenFileDialog();
            this.RTBTextoLeido = new System.Windows.Forms.RichTextBox();
            this.registroUno = new System.Windows.Forms.NumericUpDown();
            this.registroDos = new System.Windows.Forms.NumericUpDown();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtResutado = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.registroUno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.registroDos)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(685, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            this.archivoToolStripMenuItem.Click += new System.EventHandler(this.archivoToolStripMenuItem_Click);
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.abrirToolStripMenuItem.Text = "Abrir";
            this.abrirToolStripMenuItem.Click += new System.EventHandler(this.abrirToolStripMenuItem_Click);
            // 
            // OFDTexto
            // 
            this.OFDTexto.Filter = "Archivos de texto|*.txt|Valores separados por comas|*.csv|Todos los archivos|*.*";
            // 
            // RTBTextoLeido
            // 
            this.RTBTextoLeido.Location = new System.Drawing.Point(0, 25);
            this.RTBTextoLeido.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.RTBTextoLeido.Name = "RTBTextoLeido";
            this.RTBTextoLeido.Size = new System.Drawing.Size(469, 162);
            this.RTBTextoLeido.TabIndex = 1;
            this.RTBTextoLeido.Text = "";
            this.RTBTextoLeido.TextChanged += new System.EventHandler(this.RTBTextoLeido_TextChanged);
            // 
            // registroUno
            // 
            this.registroUno.Location = new System.Drawing.Point(488, 47);
            this.registroUno.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.registroUno.Name = "registroUno";
            this.registroUno.Size = new System.Drawing.Size(90, 20);
            this.registroUno.TabIndex = 2;
            this.registroUno.ValueChanged += new System.EventHandler(this.registroUno_ValueChanged);
            // 
            // registroDos
            // 
            this.registroDos.Location = new System.Drawing.Point(488, 80);
            this.registroDos.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.registroDos.Name = "registroDos";
            this.registroDos.Size = new System.Drawing.Size(90, 20);
            this.registroDos.TabIndex = 3;
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(603, 67);
            this.btnCalcular.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(56, 19);
            this.btnCalcular.TabIndex = 4;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(488, 132);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Distancia=";
            // 
            // txtResutado
            // 
            this.txtResutado.Location = new System.Drawing.Point(548, 129);
            this.txtResutado.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtResutado.Name = "txtResutado";
            this.txtResutado.ReadOnly = true;
            this.txtResutado.Size = new System.Drawing.Size(76, 20);
            this.txtResutado.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 221);
            this.Controls.Add(this.txtResutado);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.registroDos);
            this.Controls.Add(this.registroUno);
            this.Controls.Add(this.RTBTextoLeido);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.registroUno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.registroDos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog OFDTexto;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private System.Windows.Forms.RichTextBox RTBTextoLeido;
        private System.Windows.Forms.NumericUpDown registroUno;
        private System.Windows.Forms.NumericUpDown registroDos;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtResutado;
    }
}

