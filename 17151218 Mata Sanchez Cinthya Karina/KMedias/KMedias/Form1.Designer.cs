﻿namespace KMedias
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OFDTexto = new System.Windows.Forms.OpenFileDialog();
            this.RTBTextoLeido = new System.Windows.Forms.RichTextBox();
            this.registroUno = new System.Windows.Forms.NumericUpDown();
            this.registroDos = new System.Windows.Forms.NumericUpDown();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtResutado = new System.Windows.Forms.TextBox();
            this.btnAleatorio = new System.Windows.Forms.Button();
            this.numCentroides = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCalCen = new System.Windows.Forms.Button();
            this.RTBSeleccionados = new System.Windows.Forms.RichTextBox();
            this.RTBResultados = new System.Windows.Forms.RichTextBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.registroUno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.registroDos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCentroides)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(913, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(71, 24);
            this.archivoToolStripMenuItem.Text = "Archivo";
            this.archivoToolStripMenuItem.Click += new System.EventHandler(this.archivoToolStripMenuItem_Click);
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(117, 26);
            this.abrirToolStripMenuItem.Text = "Abrir";
            this.abrirToolStripMenuItem.Click += new System.EventHandler(this.abrirToolStripMenuItem_Click);
            // 
            // OFDTexto
            // 
            this.OFDTexto.FileName = "openFileDialog1";
            this.OFDTexto.Filter = "Archivos de texto | *.txt | Valores separados por comas | *.csv | Todos los archi" +
    "vos|*.*";
            // 
            // RTBTextoLeido
            // 
            this.RTBTextoLeido.Location = new System.Drawing.Point(0, 42);
            this.RTBTextoLeido.Name = "RTBTextoLeido";
            this.RTBTextoLeido.Size = new System.Drawing.Size(609, 166);
            this.RTBTextoLeido.TabIndex = 1;
            this.RTBTextoLeido.Text = "";
            this.RTBTextoLeido.TextChanged += new System.EventHandler(this.RTBTextoLeido_TextChanged);
            // 
            // registroUno
            // 
            this.registroUno.Location = new System.Drawing.Point(650, 97);
            this.registroUno.Name = "registroUno";
            this.registroUno.Size = new System.Drawing.Size(120, 22);
            this.registroUno.TabIndex = 2;
            // 
            // registroDos
            // 
            this.registroDos.Location = new System.Drawing.Point(651, 137);
            this.registroDos.Name = "registroDos";
            this.registroDos.Size = new System.Drawing.Size(120, 22);
            this.registroDos.TabIndex = 3;
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(790, 114);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 4;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(685, 180);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Distancia=";
            // 
            // txtResutado
            // 
            this.txtResutado.Location = new System.Drawing.Point(765, 177);
            this.txtResutado.Name = "txtResutado";
            this.txtResutado.ReadOnly = true;
            this.txtResutado.Size = new System.Drawing.Size(100, 22);
            this.txtResutado.TabIndex = 6;
            // 
            // btnAleatorio
            // 
            this.btnAleatorio.Location = new System.Drawing.Point(769, 269);
            this.btnAleatorio.Name = "btnAleatorio";
            this.btnAleatorio.Size = new System.Drawing.Size(96, 23);
            this.btnAleatorio.TabIndex = 10;
            this.btnAleatorio.Text = "Aleatorios";
            this.btnAleatorio.UseVisualStyleBackColor = true;
            this.btnAleatorio.Click += new System.EventHandler(this.btnAleatorio_Click);
            // 
            // numCentroides
            // 
            this.numCentroides.Location = new System.Drawing.Point(698, 276);
            this.numCentroides.Name = "numCentroides";
            this.numCentroides.Size = new System.Drawing.Size(49, 22);
            this.numCentroides.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(630, 275);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Número:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(685, 234);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "CALCULO CENTROIDES";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(662, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(203, 39);
            this.label4.TabIndex = 12;
            this.label4.Text = "CALCULO DISTANCIA ENTRE REGISTROS";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCalCen
            // 
            this.btnCalCen.Location = new System.Drawing.Point(769, 302);
            this.btnCalCen.Name = "btnCalCen";
            this.btnCalCen.Size = new System.Drawing.Size(96, 23);
            this.btnCalCen.TabIndex = 14;
            this.btnCalCen.Text = "Calcular";
            this.btnCalCen.UseVisualStyleBackColor = true;
            this.btnCalCen.Click += new System.EventHandler(this.btnCalCen_Click);
            // 
            // RTBSeleccionados
            // 
            this.RTBSeleccionados.Location = new System.Drawing.Point(0, 234);
            this.RTBSeleccionados.Name = "RTBSeleccionados";
            this.RTBSeleccionados.Size = new System.Drawing.Size(609, 91);
            this.RTBSeleccionados.TabIndex = 15;
            this.RTBSeleccionados.Text = "";
            // 
            // RTBResultados
            // 
            this.RTBResultados.Location = new System.Drawing.Point(0, 376);
            this.RTBResultados.Name = "RTBResultados";
            this.RTBResultados.Size = new System.Drawing.Size(609, 170);
            this.RTBResultados.TabIndex = 16;
            this.RTBResultados.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 603);
            this.Controls.Add(this.RTBResultados);
            this.Controls.Add(this.RTBSeleccionados);
            this.Controls.Add(this.btnCalCen);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnAleatorio);
            this.Controls.Add(this.numCentroides);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtResutado);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.registroDos);
            this.Controls.Add(this.registroUno);
            this.Controls.Add(this.RTBTextoLeido);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.registroUno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.registroDos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCentroides)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog OFDTexto;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private System.Windows.Forms.RichTextBox RTBTextoLeido;
        private System.Windows.Forms.NumericUpDown registroUno;
        private System.Windows.Forms.NumericUpDown registroDos;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtResutado;
        private System.Windows.Forms.Button btnAleatorio;
        private System.Windows.Forms.NumericUpDown numCentroides;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnCalCen;
        private System.Windows.Forms.RichTextBox RTBSeleccionados;
        private System.Windows.Forms.RichTextBox RTBResultados;
    }
}

