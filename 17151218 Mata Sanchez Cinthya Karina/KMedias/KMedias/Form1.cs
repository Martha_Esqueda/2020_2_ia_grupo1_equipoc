﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KMedias
{
    public partial class Form1 : Form
    {
        Random random ;
        LectorDatos MiLectorDatos;      
        List<Registro> Centroides = new List<Registro>();
        string datos="";
        public Form1()
        {
            InitializeComponent();
        }

        private void archivoToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OFDTexto.ShowDialog() == DialogResult.OK)
            {
                MiLectorDatos = new LectorDatos(OFDTexto.FileName);
                RTBTextoLeido.Text = MiLectorDatos.GetTextoLeido();
            }
        }

        private void RTBTextoLeido_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            //for (int i = 0; i < busquedaRegistros.Registros.Count; i++)
            //{
            //    datos+= busquedaRegistros.Registros[i].ToString();
            //}
            //RTBTextoLeido.Text = datos;
            txtResutado.Text = "" + MiLectorDatos.Registros.ElementAt((int)registroUno.Value-1).CalcularDistancia
                (MiLectorDatos.Registros.ElementAt((int)registroDos.Value-1));

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btnAleatorio_Click(object sender, EventArgs e)
        {
           random=new Random();
            string aleatorios="";
            for (int i = 0; i < numCentroides.Value; i++)
            {
               
                Centroides.Add(MiLectorDatos.Registros.ElementAt((int)random.Next(0, MiLectorDatos.Registros.Count)));
                aleatorios += Centroides[i].getDatos()+"\n";
            }
            RTBSeleccionados.Text = aleatorios;
        }

        private void btnCalCen_Click(object sender, EventArgs e)
        {
            string valores = "";
            for (int i = 0; i < Centroides.Count; i++)
            {
                List<float> dist = new List<float>();
                dist=Centroides[i].CalcularCentroides(MiLectorDatos.Registros);
                for (int x = 0; x < dist.Count; x++)
                {
                    valores += dist[x].ToString() + "\n";
                }
                valores += "\n";
            }
            RTBResultados.Text = valores;
        }        
    }
}
