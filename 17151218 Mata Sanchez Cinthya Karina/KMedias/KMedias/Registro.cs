﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KMedias
{
    class Registro
    {
        string Identificador;
        List<float> Datos=new List<float>();       
        string campo="";
        Boolean primerDato=false;
        int numDatos=0;              
        
        public Registro(string StringFuente)
        {
            //Va a extraer los datos del StringFuente y los establece 
            //en la lista de datos           
            for (int i = 0; i < StringFuente.Length; i++)
            {
                if (StringFuente.ElementAt(i) != ',')
                {
                    campo += StringFuente.ElementAt(i);
                }
                else
                {
                    if (!primerDato)
                    {
                        Identificador = campo;
                       // MessageBox.Show(Identificador);                        
                        primerDato = true;                       
                    }
                    else
                    {
                        if (campo != "")
                        {
                            //MessageBox.Show(""+float.Parse(campo));
                            Datos.Add(float.Parse(campo));
                        }
                    }
                    
                    campo = "";
                    //MessageBox.Show(campo);
                }
            }
            Datos.Add(float.Parse(campo));
            campo = "";
            // MessageBox.Show(Identificador +"\n"+Datos);
            primerDato = false;           
        }

        public string getDatos()
        {
            string registro="";
            for (int i = 0; i < Datos.Count; i++)
            {
                registro += Datos[i].ToString()+" ";
            }
            return registro;
        }

        public float CalcularDistancia(Registro OtroRegistro)
        {
            float sumaDatos = 0f;
            float distancia = 0f;
            List<float> restasDatos = new List<float>();
            for (int i = 0; i < Datos.Count; i++)
            {
                restasDatos.Add(Datos[i] - OtroRegistro.Datos[i]);
                //MessageBox.Show(Datos[i]+"-"+OtroRegistro.Datos[i]+restasDatos[i]+"");
            }
            for (int i = 0; i < Datos.Count; i++)
            {
                sumaDatos += ((float)Math.Pow(restasDatos[i], 2));
            }
            distancia=(float)Math.Sqrt(sumaDatos);
            return distancia;
        }

        public List<float> CalcularCentroides(List<Registro> Cluster)
        {
            float sumaDatos = 0f;
            float distancia = 0f;
            List<float> restasDatos = new List<float>();
            List<float> distCentroide = new List<float>();
            for (int i = 0; i < Cluster.Count ; i++)
            {
                for (int x = 0; x < Datos.Count; x++)
                {
                    restasDatos.Add(Datos[x] - Cluster[i].Datos[x]);                   
                }
                for (int y=0; y < Datos.Count; y++)
                {
                    sumaDatos += ((float)Math.Pow(restasDatos[y], 2));
                }
                distancia = (float)Math.Sqrt(sumaDatos);
                distCentroide.Add(distancia);
                distancia = 0f;
            }
            return distCentroide;
        }
    }
}
