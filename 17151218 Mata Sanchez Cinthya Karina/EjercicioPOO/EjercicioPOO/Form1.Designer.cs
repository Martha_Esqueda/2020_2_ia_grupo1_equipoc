﻿namespace EjercicioPOO
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtClave = new System.Windows.Forms.TextBox();
            this.btnInscribir = new System.Windows.Forms.Button();
            this.btnAgregarMat = new System.Windows.Forms.Button();
            this.RTBMaterias = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtClaveMateriaCursar = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.NUDCalificacion = new System.Windows.Forms.NumericUpDown();
            this.btnCursarMateria = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NUDCalificacion)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre del Alumno:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Clave del Alumno:";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(168, 29);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(250, 22);
            this.txtNombre.TabIndex = 2;
            // 
            // txtClave
            // 
            this.txtClave.Location = new System.Drawing.Point(168, 71);
            this.txtClave.Name = "txtClave";
            this.txtClave.Size = new System.Drawing.Size(250, 22);
            this.txtClave.TabIndex = 3;
            // 
            // btnInscribir
            // 
            this.btnInscribir.Location = new System.Drawing.Point(122, 141);
            this.btnInscribir.Name = "btnInscribir";
            this.btnInscribir.Size = new System.Drawing.Size(142, 40);
            this.btnInscribir.TabIndex = 4;
            this.btnInscribir.Text = "Inscribir Alumno";
            this.btnInscribir.UseVisualStyleBackColor = true;
            this.btnInscribir.Click += new System.EventHandler(this.btnInscribir_Click);
            // 
            // btnAgregarMat
            // 
            this.btnAgregarMat.Enabled = false;
            this.btnAgregarMat.Location = new System.Drawing.Point(289, 141);
            this.btnAgregarMat.MinimumSize = new System.Drawing.Size(140, 40);
            this.btnAgregarMat.Name = "btnAgregarMat";
            this.btnAgregarMat.Size = new System.Drawing.Size(140, 40);
            this.btnAgregarMat.TabIndex = 5;
            this.btnAgregarMat.Text = "Agregar Materia";
            this.btnAgregarMat.UseVisualStyleBackColor = true;
            this.btnAgregarMat.Click += new System.EventHandler(this.btnAgregarMat_Click);
            // 
            // RTBMaterias
            // 
            this.RTBMaterias.Location = new System.Drawing.Point(98, 219);
            this.RTBMaterias.Name = "RTBMaterias";
            this.RTBMaterias.ReadOnly = true;
            this.RTBMaterias.Size = new System.Drawing.Size(331, 190);
            this.RTBMaterias.TabIndex = 6;
            this.RTBMaterias.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 219);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Materias:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 435);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Clave de materia a cursar:";
            // 
            // txtClaveMateriaCursar
            // 
            this.txtClaveMateriaCursar.Location = new System.Drawing.Point(207, 435);
            this.txtClaveMateriaCursar.Name = "txtClaveMateriaCursar";
            this.txtClaveMateriaCursar.Size = new System.Drawing.Size(193, 22);
            this.txtClaveMateriaCursar.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 487);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(210, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Calificación de materia a cursar:";
            // 
            // NUDCalificacion
            // 
            this.NUDCalificacion.Location = new System.Drawing.Point(242, 482);
            this.NUDCalificacion.Name = "NUDCalificacion";
            this.NUDCalificacion.Size = new System.Drawing.Size(120, 22);
            this.NUDCalificacion.TabIndex = 11;
            // 
            // btnCursarMateria
            // 
            this.btnCursarMateria.Location = new System.Drawing.Point(287, 527);
            this.btnCursarMateria.Name = "btnCursarMateria";
            this.btnCursarMateria.Size = new System.Drawing.Size(142, 40);
            this.btnCursarMateria.TabIndex = 12;
            this.btnCursarMateria.Text = "Cursar Materia";
            this.btnCursarMateria.UseVisualStyleBackColor = true;
            this.btnCursarMateria.Click += new System.EventHandler(this.btnCursarMateria_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 593);
            this.Controls.Add(this.btnCursarMateria);
            this.Controls.Add(this.NUDCalificacion);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtClaveMateriaCursar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.RTBMaterias);
            this.Controls.Add(this.btnAgregarMat);
            this.Controls.Add(this.btnInscribir);
            this.Controls.Add(this.txtClave);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Datos del Alumno";
            ((System.ComponentModel.ISupportInitialize)(this.NUDCalificacion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtClave;
        private System.Windows.Forms.Button btnInscribir;
        private System.Windows.Forms.Button btnAgregarMat;
        private System.Windows.Forms.RichTextBox RTBMaterias;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtClaveMateriaCursar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown NUDCalificacion;
        private System.Windows.Forms.Button btnCursarMateria;
    }
}

