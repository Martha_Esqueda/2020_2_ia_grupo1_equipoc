﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlgoritmoGenetico
{
    public partial class Form1 : Form
    {
        Poblacion LaPoblacion;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnInicializar_Click(object sender, EventArgs e)
        {
            LaPoblacion = new Poblacion((int)NUDTamPoblacion.Value, new EvaluadorTipo1("123456789"));
            RTBPoblacion.Text = LaPoblacion.GetDatos();
            LaPoblacion.SelecionSimple((int)NUDPresion.Value);
            RTBSeleccionados.Text = LaPoblacion.GetSeleccion();
        }
    }
}
