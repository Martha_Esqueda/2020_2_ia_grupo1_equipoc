﻿namespace AlgoritmoGenetico
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnInicializar = new System.Windows.Forms.Button();
            this.RTBPoblacion = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.NUDTamPoblacion = new System.Windows.Forms.NumericUpDown();
            this.RTBSeleccionados = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.NUDPresion = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NUDTamPoblacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDPresion)).BeginInit();
            this.SuspendLayout();
            // 
            // btnInicializar
            // 
            this.btnInicializar.Location = new System.Drawing.Point(427, 12);
            this.btnInicializar.Name = "btnInicializar";
            this.btnInicializar.Size = new System.Drawing.Size(115, 58);
            this.btnInicializar.TabIndex = 0;
            this.btnInicializar.Text = "Inicializar población";
            this.btnInicializar.UseVisualStyleBackColor = true;
            this.btnInicializar.Click += new System.EventHandler(this.btnInicializar_Click);
            // 
            // RTBPoblacion
            // 
            this.RTBPoblacion.Location = new System.Drawing.Point(16, 99);
            this.RTBPoblacion.Name = "RTBPoblacion";
            this.RTBPoblacion.Size = new System.Drawing.Size(250, 353);
            this.RTBPoblacion.TabIndex = 1;
            this.RTBPoblacion.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tamaño población:";
            // 
            // NUDTamPoblacion
            // 
            this.NUDTamPoblacion.Location = new System.Drawing.Point(148, 33);
            this.NUDTamPoblacion.Name = "NUDTamPoblacion";
            this.NUDTamPoblacion.Size = new System.Drawing.Size(75, 22);
            this.NUDTamPoblacion.TabIndex = 3;
            // 
            // RTBSeleccionados
            // 
            this.RTBSeleccionados.Location = new System.Drawing.Point(299, 166);
            this.RTBSeleccionados.Name = "RTBSeleccionados";
            this.RTBSeleccionados.Size = new System.Drawing.Size(243, 181);
            this.RTBSeleccionados.TabIndex = 4;
            this.RTBSeleccionados.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(244, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Presion:";
            // 
            // NUDPresion
            // 
            this.NUDPresion.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.NUDPresion.Location = new System.Drawing.Point(304, 33);
            this.NUDPresion.Name = "NUDPresion";
            this.NUDPresion.Size = new System.Drawing.Size(75, 22);
            this.NUDPresion.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(382, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "%";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(301, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(156, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Lista de seleccionados:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Población:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 488);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.NUDPresion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.RTBSeleccionados);
            this.Controls.Add(this.NUDTamPoblacion);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RTBPoblacion);
            this.Controls.Add(this.btnInicializar);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.NUDTamPoblacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDPresion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnInicializar;
        private System.Windows.Forms.RichTextBox RTBPoblacion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown NUDTamPoblacion;
        private System.Windows.Forms.RichTextBox RTBSeleccionados;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown NUDPresion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}

