﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoritmoGenetico
{
    class Poblacion
    {
        List<Individuo> Individuos;
        List<Individuo> individuosSeleccionados;

        public Poblacion(int TamPoblacion, Evaluador ElEvaluador)
        {
            Individuos = new List<Individuo>();
            Random GeneradorAleatorios = new Random();

            for (int i = 0; i < TamPoblacion; i++)
                Individuos.Add(new Individuo(ElEvaluador, GeneradorAleatorios));
        }

        public string GetDatos()
        {
            StringBuilder datos = new StringBuilder();

            for (int i = 0; i < Individuos.Count; i++)
                datos.Append(Individuos[i].GetDatos() + "\n");

            return datos.ToString();
        }

        public string GetSeleccion()
        {
            StringBuilder datos = new StringBuilder();

            for (int i = 0; i < individuosSeleccionados.Count; i++)
                datos.Append(individuosSeleccionados[i].GetDatos() + "\n");

            return datos.ToString();
        }

        public void SelecionSimple(int Presion)
        {
            individuosSeleccionados = new List<Individuo>();

            int cantidad_seleccionados = (100 - Presion) * Individuos.Count / 100;
            
            for (int x = 9; x > 0; x--)
            {
                for (int i = 0; i < Individuos.Count; i++)
                {
                    if (individuosSeleccionados.Count < cantidad_seleccionados)
                    {
                        if (Individuos[i].GetFitness() == x)
                        {
                            individuosSeleccionados.Add(new Individuo(Individuos[i].GetGenotipo(), Individuos[i].GetFitness()));
                        }
                    }
                }
            }
        }
    }
}
