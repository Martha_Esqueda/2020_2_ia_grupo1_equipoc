﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgoritmoGenetico
{
    class Individuo
    {
        string Genotipo;
        int Fitness;

        Evaluador ElEvaluador;

        public Individuo(Evaluador ElEvaluador, Random GeneradorAleatorios)
        {
            this.ElEvaluador = ElEvaluador;
            Genotipo = "";
            Fitness = 0;

            for (int i = 0; i < ElEvaluador.GetModelo().Length; i++)
                Genotipo += GeneradorAleatorios.Next(0, 9);

            Fitness = ElEvaluador.Evaluar(Genotipo);
        }

        public Individuo(string Genotipo, int Fitness)
        {
            this.Genotipo = Genotipo;
            this.Fitness = Fitness;
        }

        public string GetDatos()
        {
            return Genotipo + " = " + Fitness;
        }

        public int GetFitness()
        {
            return Fitness;
        }

        public string GetGenotipo()
        {
            return Genotipo;
        }
    }
}
