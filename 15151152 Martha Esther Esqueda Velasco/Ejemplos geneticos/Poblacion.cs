﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplos_geneticos
{
    class Poblacion
    {
        List<individuo> Individuos;
        public Poblacion(int TamPoblacion)
        {
            Individuos = new List<individuo>();
            for (int i = 0; i < TamPoblacion; i++)
                Individuos.Add(new Individuo(ElEvaluador));

  }
        public string GetDatos()
        {
            StringBuilder datos = new StringBuilder();
            for (int i = 0; i < Individuos.Count; i++)
                datos.Append(Individuos[i].GetDatos() + "\n");

            return datos.ToString();
        }
    }
}
