﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjerciciosPOO
{
    class Alumno
    {
        string NumControl;
        string Nombre;
        string Carrera;
        string Escuela;

        int Semestre;
        int Promedio;
        int Creditos;

        bool Sexo;
        int Edad;

        List<Materia> Materias;

        public Alumno(string NumControl, string Nombre)
        {
            this.NumControl = NumControl;
            this.Nombre = Nombre;
            Semestre = 1;
            Promedio = 0;
            Creditos = 0;

        }
    }
}
