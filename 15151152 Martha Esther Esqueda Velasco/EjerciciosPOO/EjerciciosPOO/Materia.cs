﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjerciciosPOO
{
    class Materia
    {
        string Clave;
        string Nombre;
        string Profesor;
        string Departamento;

        int Creditos;
        int Calificacion;

        bool EsDeEpecialidad;
        int intentos;

        public Materia(string Clave, string Nombre, int Creditos)
        {
            this.Clave = Clave;
            this.Nombre = Nombre;
            this.Creditos = Creditos;
            Calificacion = 0;
            intentos = 1;

        }
        public bool CursarMateria (int Calificacion)
        {
            if (IntentosAgotados())
                return false;

            if (Calificacion < 70)
            {
                Calificacion = 0;
                intentos++;
            }
            else
            {
                this.Calificacion = Calificacion;
            }
            return true;   

        }
        public bool YaEstaAprobada()
        {
            return Calificacion > 69;

        }
        public bool IntentosAgotados()
        {
            return intentos > 3;

        }
        public bool YaCursoMateria()
        {
            return intentos > 1 || YaEstaAprobada();

        }
    }
}
