﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioPoo
{
    class Materia
    {
        string Clave;
        string Nombre;
        string Profesor;
        string Departamento;

        int Creditos;
        int Calificacion;
        int Intentos;

        bool EsDeEspecialidad;

        public Materia(string Clave, string Nombre, int Creditos)
        {
            this.Clave = Clave;
            this.Nombre = Nombre;
            this.Creditos = Creditos;
            Calificacion = 0;

            Intentos = 1;
        }

        public bool CursarMateria(int Calificacion)
        {
            if (IntentosAgotados())

                return false;

            if (Calificacion < 70)
            {
                Calificacion = 0;
                Intentos++;
            }
            else
            {
                this.Calificacion = Calificacion;
            }

                return true;
        }

        public bool YaEstaAprobada()
        {
            return Calificacion > 69;
        }

        public bool IntentosAgotados()
        {
            return Intentos > 3;
        }

        public bool YaCursoLaMateria()
        {
            return Intentos > 1 || YaEstaAprobada();
        }
    }
}
