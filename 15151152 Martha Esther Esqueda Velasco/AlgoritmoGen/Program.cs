﻿using System;
using System.Globalization;
namespace algoritmoGenConsola
{
    class Program 
    {

		internal static int TAMAÑO_POBLACION;
		internal static int TAMAÑO_INDIVIDUO;
		internal static int EVALUACION;
		internal static int GENERACION;
		internal static int SUMATORIO_INDIV;
		internal static int NUM_SOLUCIONES;

		internal static double coef_mutacion;
		internal static double coef_rotacion;

		internal static int[][] poblacion;
		internal static int[] fitness;
		internal static int[] informacion_genetica;

		// Inicializamos la poblacion de forma aleatoria
		internal static void poblacionInicial()
		{
			double coef_random = 0;

			for (int k = 0; k < poblacion.Length; k++)
			{
				for (int i = 0; i < poblacion[0].Length; i++)
				{
					coef_random = GlobalRandom.NextDouble;

					if (coef_random < 0.5)
					{
						poblacion[k][i] = 0;
					}
					else
					{
						poblacion[k][i] = 1;
					}
				}
			}
		}
		//Generamos un individuo aleatorio y usamos el concepto de distancia hamming para crear los demas repartidos
		// a distintas distancias de este
		internal static void inicializa_explorando()
		{
			double coef_random = 0;
			int[] individuo = new int[TAMAÑO_INDIVIDUO];

			//Inicializamos el individuo desde el que miraremos la distancia hamming
			for (int i = 0; i < individuo.Length; i++)
			{
				coef_random = GlobalRandom.NextDouble;

				if (coef_random < 0.5)
				{
					individuo[i] = 0;
				}
				else
				{
					individuo[i] = 1;
				}
			}


			//Inicializamos la pre_poblacion cambiando n genes al individuo
			int n = 0;
			for (int k = 0; k < poblacion.Length; k++)
			{
				for (int i = 0; i < n; i++)
				{
					coef_random = GlobalRandom.NextDouble * (TAMAÑO_INDIVIDUO - n);
					poblacion[k] = (int[])individuo.Clone();

					if (individuo[i] == 0)
					{
						poblacion[k][(int)coef_random] = 1;
					}
					else
					{
						poblacion[k][(int)coef_random] = 0;
					}

				}
				n++;

				if (n == TAMAÑO_INDIVIDUO)
				{
					n = 1;
				}
			}
		}
		/*Calculamos la funcion fitness de cada individuo como la diferencia de los sumatorios de los grupos
		 El mejor fitness es el menor numero (0 es el optimo)
		*/ 
		internal static void Fitness()
		{

			EVALUACION++;

			//Declaramos los sumatorios de los grupos
			int sumatorioA;
			int sumatorioB;


			for (int i = 0; i < TAMAÑO_POBLACION; i++)
			{

				sumatorioA = 0; //0's en el genotipo
				sumatorioB = 0; //1's en el genotipo

				for (int j = 0; j < TAMAÑO_INDIVIDUO; j++)
				{

					//Obtenemos el resultado de la suma en los nodos
					if (poblacion[i][j] == 0)
					{
						sumatorioA += informacion_genetica[j];
					}
					else
					{
						sumatorioB += informacion_genetica[j];
					}

				}
				fitness[i] = sumatorioA - sumatorioB;
				if (fitness[i] < 0)
				{
					fitness[i] *= -1;
				}
			}
		}
		// <param name="numero_reemplazo">: numero de individuos que se reproduciran en la poblacion </param>
		// <returns> pos_mejores Devuelve las posiciones de los elegidos para la reproduccion
		//  </returns>
		public static int[][] Ranking(double presion_selectiva)
		{
			int[][] rank = RectangularArrays.RectangularIntArray(TAMAÑO_POBLACION, TAMAÑO_INDIVIDUO);
			//int [] pos_rank = new int [TAMAÑO_POBLACION];
			double probabilidad = 0;
			int[][] poblacion_ord;
			//int [] fitness_ord;
			int[] ruleta = new int[TAMAÑO_POBLACION];
			//Presion selectiva de ranking
			double A = presion_selectiva;
			double B = 0;
			double n = TAMAÑO_POBLACION / 8;
			//Ordenamos segun fitness
			poblacion_ord = Ordenar();
			int z = 0;
			for (int i = 0; z < ruleta.Length && n >= 0; i++)
			{
				double g = ((A * (n--) + B) * 100);
				//System.out.println(g);
				if ((g % 1.0) >= 0.5)
				{
					g = (int)((A * (n + 1) + B) * 100) + 1.0;
				}
				else if (g % 1.0 >= 0)
				{
					g = (int)((A * ((n + 1)) + B) * 100);
				}

				for (int j = 0; z < TAMAÑO_POBLACION && j < g; j++, z++)
				{
					ruleta[z] = i;
				} //System.out.println(g+" "+z+"  "+i);
			}

			//seleccionamos proporcionalmente segun el rango
			for (int i = 0; i < rank.Length; i++)
			{
				probabilidad = GlobalRandom.NextDouble * (double)TAMAÑO_POBLACION;
				if (probabilidad < 0)
				{
					probabilidad *= -1;
				}
				rank[i] = poblacion_ord[ruleta[(int)probabilidad]];
				//pos_rank[i] = ruleta[(int) probabilidad];
				//System.out.println(pos_rank[i]);

			}
			return (rank);
		}		
        // Seleccion por ruleta
		//public static int[][] Ruleta()
		//{

		//	//Definimos variables necesarias
		//	int Si = -1; //Simboliza el individuo ganador i en S(i) para el vector S
		//	int[][] poblacion_intermedia = RectangularArrays.RectangularIntArray(TAMAÑO_POBLACION, TAMAÑO_INDIVIDUO);

		//	double[] fr = new double[TAMAÑO_POBLACION]; //(fitness relativo = fitness / fitness total)
		//	double ft = 0; //Mide en cuanto falla la poblaci—n (fitness total)
		//	double[] S = new double[TAMAÑO_POBLACION]; //vector S
		//	double K; //numero aleatorio

		//	ft = 0;
		//	//Hallamos el fitness total de la poblacion
		//	for (int i = 0; i < TAMAÑO_POBLACION; i++)
		//	{
		//		ft += fitness[i];
		//	}

		//	//calculamos el fitness relativo para cada individuo
		//	for (int i = 0; i < TAMAÑO_POBLACION; i++)
		//	{
		//		if (ft != 0)
		//		{
		//			fr[i] = (double)fitness[i] / (double)ft;
		//		}
		//		else
		//		{
		//			fr[i] = 0;
		//		}

		//	}

		//	//generamos el vector de n elementos
		//	S[0] = fr[0];
		//	for (int i = 1; i < TAMAÑO_POBLACION; i++)
		//	{
		//		S[i] = fr[i] + S[i - 1];
		//	}

		//	int rellena_poblacion = 0;
		//	while (rellena_poblacion < poblacion_intermedia.Length)
		//	{

		//		//Generamos un numero aleatorio
		//		K = GlobalRandom.NextDouble;

		//		//Buscamos el menor mayor que K en S(i) comprobando que S(i-1) es menor
		//		if (K < S[0])
		//		{
		//			Si = 0;

		//		}
		//		else
		//		{
		//			for (int i = 1; i < S.Length; i++)
		//			{

		//				if (K <= S[i] && K > S[i - 1])
		//				{
		//					Si = i;
		//					i = S.Length;

		//				}
		//				else if (i == S.Length - 1)
		//				{
		//					Si = i;
		//				}
		//			}
		//		}

		//		//Seleccionamos S(i)

		//		poblacion_intermedia[rellena_poblacion] = (int[])poblacion[Si].Clone();
		//		Si = -1;
		//		//Pasamos al buscar el siguiente individuo de la poblacion intermedia
		//		rellena_poblacion++;

		//	}
		//	return (poblacion_intermedia);
		//}


        //Ordenamos los datos de la poblacion
		public static int[][] Ordenamiento()
		{
			int[] aux_i;
			int []aux_j;
			int aux_k, aux_m;
			int[][] poblacion_ord = (int[][])poblacion.Clone();
			int[] fitness_ord = (int[])fitness.Clone();

			for (int i = 0; i < fitness_ord.Length; i++)
			{
				for (int j = i + 1; j < fitness_ord.Length; j++)
				{
					if (fitness_ord[i] > fitness[j])
					{
						aux_i = poblacion_ord[j];
						aux_j = poblacion_ord[i];
						aux_k = fitness_ord[j];
						aux_m = fitness_ord[i];
						poblacion_ord[i] = aux_i;
						poblacion_ord[j] = aux_j;
						fitness_ord[i] = aux_k;
						fitness_ord[j] = aux_m;
					}
				}
			}
			return (poblacion_ord);
		}
		// Desordena la poblacion tras el ranking por medio del algoritmo FisherÐYates shuffle
		public static int[][] desordena_poblacion(int[][] pob)
		{
			int[][] poblacion_intermedia = (int[][])poblacion.Clone();

			for (int i = 0; i < poblacion_intermedia.Length; i++)
			{
				int r = i + (int)(GlobalRandom.NextDouble * (poblacion_intermedia.Length - i));
				int[] swap = (int[])poblacion_intermedia[r].Clone();
				poblacion_intermedia[r] = (int[])poblacion_intermedia[i].Clone();
				poblacion_intermedia[i] = (int[])swap.Clone();

			}
			return poblacion_intermedia;

		}
		
		internal static class GlobalRandom
		{
			private static System.Random randomInstance = null;

			public static double NextDouble
			{
				get
				{
					if (randomInstance == null)
						randomInstance = new System.Random();

					return randomInstance.NextDouble();
				}
			}
		}

        internal static class RectangularArrays
		{
			public static int[][] RectangularIntArray(int size1, int size2)
			{
				int[][] newArray = new int[size1][];
				for (int array1 = 0; array1 < size1; array1++)
				{
					newArray[array1] = new int[size2];
				}

				return newArray;
			}
		}

		public static int[][] Cruce(int[][] pob, int porcent)
		{
			int[][] pob_intermedia = RectangularArrays.RectangularIntArray(pob.Length, pob[0].Length);

			for (int i = 0, j = pob.Length - 1; i < pob.Length / 2; i++, j--)
			{

				int k = 0;
				for (; k < TAMAÑO_INDIVIDUO / 2; k++)
				{
					pob_intermedia[i][k] = pob[i][k];
					pob_intermedia[j][k] = pob[j][k];
				}

				for (; k < TAMAÑO_INDIVIDUO; k++)
				{
					pob_intermedia[j][k] = pob[i][k];
					pob_intermedia[i][k] = pob[j][k];
				}
			}
			return pob_intermedia;
		}

        public static int[][] Mutacion(int[][] no_mutada)
		{

			double rdn = GlobalRandom.NextDouble;

			for (int j = 0; j < no_mutada.Length; j++)
			{

				for (int i = 0; i < TAMANYO_INDIVIDUO; i++)
				{

					if (rdn <= coef_mutacion)
					{
						if (no_mutada[j][i] == 0)
						{
							no_mutada[j][i] = 1;
						}
						else
						{
							no_mutada[j][i] = 0;
						}

					}
					rdn = GlobalRandom.NextDouble;
				}

			}
			if (coef_mutacion > 0.002)
			{
				coef_mutacion /= (double)1.0001;
			}

			return no_mutada;
		}

		public static int[][] Mut_rotacion(int[][] no_rotada)
		{

			double rnd_rota = GlobalRandom.NextDouble;

			//Rotacion aleatoria
			//int tamanyo_rotacion = (int)(TAMANYO_INDIVIDUO*coef_mutacion);
			//double empieza_rotacion = Math.random()*(TAMANYO_INDIVIDUO-tamanyo_rotacion);

			//Rotacion fija variable
			int tamanyo_rotacion = (int)(TAMANYO_INDIVIDUO * coef_mutacion);
			double empieza_rotacion = (int)(TAMANYO_INDIVIDUO / 3);

			//Rotacion fija estatica
			//int tamanyo_rotacion = TAMANYO_INDIVIDUO/4;
			//double empieza_rotacion = (int)(TAMANYO_INDIVIDUO/4);

			int[][] rota = (int[][])no_rotada.Clone();

			for (int j = 0; j < no_rotada.Length; j++)
			{
				for (int i = 0; i < tamanyo_rotacion; i++)
				{
					if (rnd_rota <= coef_rotacion)
					{
						rota[j][(int)(empieza_rotacion + tamanyo_rotacion - 1 - i)] = no_rotada[j][(int)(empieza_rotacion - 1) + i];
					}
					rnd_rota = GlobalRandom.NextDouble;
				}

			}
			if (coef_mutacion > 0.002)
			{
				coef_mutacion /= (double)1.0001;
			}
			if (coef_rotacion > 0.002)
			{
				coef_rotacion /= (double)1.0005;
			}

			return no_rotada;
		}

		public static void Main(string[] args)
		{
			//Definimos las salidas finales del programa
			double GEN_MEDIA = 0;
			int MEJOR = 0;
			double MEJOR_MEDIA = 0;
			double EV_MEDIA = 0;
			double SOLUCIONES_MEDIA = 0;
			//Definimos auxiliares para las salidas
			Console.WriteLine("Ingrese tamaño de la poblacion....");
			TAMANYO_POBLACION = Convert.ToInt16(Console.ReadLine());
			Console.WriteLine("Ingrese cuantos individuos");
			TAMANYO_INDIVIDUO = Convert.ToInt16(Console.ReadLine());
			int[] MEJORES = new int[10];
			int[] GENERACIONES = new int[10];
			int[] EVS_MEDIA = new int[10];
			int[] SOLS_ENC = new int[10];
			for (int ejecucion = 0; ejecucion < TAMANYO_POBLACION; ejecucion++)
			{
				Console.WriteLine("Poblacion " + (ejecucion + 1));
				GENERACION = 0;
				EVALUACION = 0;
				NUM_SOLUCIONES = TAMANYO_POBLACION;
				coef_mutacion = 0.20;
				coef_rotacion = 0.20;
				coef_rotacion = 0.20;

				int gen_valida = 0;
				int soluciones_encontradas = 0;
				
				int[][] soluciones = RectangularArrays.RectangularIntArray(NUM_SOLUCIONES, TAMANYO_INDIVIDUO + 1);

				//Inicializamos matrices de poblacion, fitness e informacion genetica
		
				poblacion = RectangularArrays.RectangularIntArray(TAMANYO_POBLACION, TAMANYO_INDIVIDUO);
				fitness = new int[TAMANYO_POBLACION];
				informacion_genetica = new int[TAMANYO_INDIVIDUO];

				Console.WriteLine();

				//Pruebas con individuos aleatorios de 50 bits
				int[] info_dom = new int[TAMANYO_INDIVIDUO];
				for (int i = 0; i < info_dom.Length; i++)
				{
					info_dom[i] = (int)(GlobalRandom.NextDouble * 10);
					if (i < info_dom.Length - 1)
					{
						Console.Write(info_dom[i] + "  ");
					}
					else
					{
						Console.Write(info_dom[i]);
					}

				}
				informacion_genetica = info_dom;

				//Obtenemos el peor valor posible de un fitness
				for (int i = 0; i < TAMANYO_INDIVIDUO; i++)
				{
					SUMATORIO_INDIV += informacion_genetica[i];
				}
				//El mejor valor posible es 0

				//Inicializamos la poblacion inicial
				poblacionInicial();

				inicializa_explorando();

				//Hallamos la funcion fitness inicial de cada individuo
				fFitness();

				//Definimos una variable para que, si se lleva un numero determinado de generaciones sin encontrar resultado nuevo, se pare
				bool no_halla_mas = false;

				//Comenzamos con las generaciones hasta un maximo de 20 si no alcanza oto criterio de parada
				while (soluciones_encontradas == 0 && GENERACION < 20 && (GENERACION - gen_valida) < 10 && !no_halla_mas)
				{

					//Seleccion con ranking
					double top_ranking = (double)1/(double)(TAMANYO_POBLACION*0.8);
					int [][] pob = Ranking(top_ranking);

					//Desordenamos la poblacion aleatoriamente para que el cruce no sea estatico
					pob = desordena_poblacion(pob);

					//Cruce simple
					poblacion = (int[][])Cruce(pob, TAMANYO_POBLACION).Clone();


					//Realizamos la mutacion
					poblacion = Mutacion((int[][])poblacion.Clone());

					//Hallamos la funcion fitness de cada individuo
					fFitness();
					for (int j = 0; j < poblacion.Length && soluciones_encontradas == 0; j++)
					{

						//Si son resultado, entran
						if (fitness[j] == 0 || (SUMATORIO_INDIV % 2 == 1 && fitness[j] == 1))
						{

							//Miramos que las soluciones no sean repetidas	
							bool no_cuenta = false;
							for (int k = 0; k < soluciones_encontradas; k++)
							{
								bool igual = true;
								for (int z = 0; z < TAMANYO_INDIVIDUO; z++)
								{
									if (poblacion[j][z] != soluciones[k][z])
									{
										igual = false;
									}

								}
								if (igual)
								{
									no_cuenta = true;
								}
							}

							//Si la solucion no es repetida, se almacena y se sustituye en la poblacion
							if (!no_cuenta)
							{
								for (int k = 0; k < TAMANYO_INDIVIDUO; k++)
								{
									soluciones[soluciones_encontradas][k] = poblacion[j][k];
								}
								soluciones[soluciones_encontradas][TAMANYO_INDIVIDUO] = GENERACION;

								soluciones_encontradas++;
								gen_valida = GENERACION;

								//Cambiamos la solucion correcta por un nuevo individuo para evitar elitismo
								double rdn = GlobalRandom.NextDouble;
								for (int k = 0; k < poblacion[j].Length; k++)
								{
									rdn = GlobalRandom.NextDouble;
									if (rdn < 0.5)
									{
										poblacion[j][k] = 0;
									}
									else
									{
										poblacion[j][k] = 1;
									}
								}

								//hallamos el nuevo fitness de dicho individuo y se cambia en el vector de fitness global
								int sumatorioA = 0; //0's en el genotipo
								int sumatorioB = 0; //1's en el genotipo

								for (int k = 0; k < TAMANYO_INDIVIDUO; k++)
								{

									//Obtenemos el resultado de la suma en los nodos
									if (poblacion[j][k] == 0)
									{
										sumatorioA += informacion_genetica[k];
									}
									else
									{
										sumatorioB += informacion_genetica[k];
									}

								}
								fitness[j] = sumatorioA - sumatorioB;
								if (fitness[j] < 0)
								{
									fitness[j] *= -1;
								}
							}
						}
					}
					//Si lleva muchas generaciones sin encontrar soluciones y ya tenemos alguna, paramos
					//Si lleva 20000 generaciones sin encontrar solucion, paramos y devolvemos las mejores
					if ((GENERACION - gen_valida == 9999 && soluciones_encontradas > 1) || (GENERACION - gen_valida == 20000))
					{
						//Si paramos por este criterio, imprimimos el ranking
						no_halla_mas = true;
						int tamanyo_ranking = TAMANYO_POBLACION / 4;
						Console.WriteLine(tamanyo_ranking + " soluciones proximas:");
						poblacion = Ordenar();
						fFitness();
						for (int z = 0; z < tamanyo_ranking; z++)
						{
							Console.Write("N¼ " + (z + 1) + " Fitness: " + fitness[z] + " individuo: ");
							for (int j = 0; j < TAMANYO_INDIVIDUO; j++)
							{
								Console.Write(poblacion[z][j]);
							}
							Console.WriteLine(" ");
						}
					}

					GENERACION++;
				}

				//Imprimimos las soluciones
				Console.WriteLine();
				Console.WriteLine("Numero de evaluaciones hechas: " + EVALUACION);
				Console.WriteLine("Numero de generaciones hechas: " + GENERACION);

				if (soluciones_encontradas > 0)
				{
					if (SUMATORIO_INDIV % 2 == 0)
					{
						Console.WriteLine("La mejor solucion es 0");
						MEJORES[ejecucion] = 0;
						MEJOR = 0;
					}
					else
					{
						Console.WriteLine("La mejor solucion es: 1 (el numero es impar)");
						MEJORES[ejecucion] = 1;
						MEJOR = 1;
					}

				}
				else
				{
					poblacion = Ordenar();
					fFitness();
					Console.WriteLine("La mejor solucion es " + fitness[0] );
					MEJORES[ejecucion] = fitness[0];
					if (fitness[0] < MEJOR)
					{
						MEJOR = fitness[0];
					}
				}
				Console.WriteLine();
				for (int i = 0; i < soluciones_encontradas; i++)
				{
					Console.Write("La solucion " + i + " es: ");
					for (int j = 0; j < TAMANYO_INDIVIDUO; j++)
					{
						Console.Write(soluciones[i][j]);
					}
					Console.Write(" ---> Hallada en la generacion ");
					Console.WriteLine(soluciones[i][TAMANYO_INDIVIDUO]);
				}
				GENERACIONES[ejecucion] = GENERACION;
				EVS_MEDIA[ejecucion] = EVALUACION;
				SOLS_ENC[ejecucion] = soluciones_encontradas;

			}

			Console.WriteLine();

			//IMPRIMIMOS LAS SALIDAS FINALES	

			for (int i = 0; i < GENERACIONES.Length; i++)
			{
				GEN_MEDIA += (double)GENERACIONES[i];

			}
			GEN_MEDIA = (double)GEN_MEDIA / (double)GENERACIONES.Length;

			for (int i = 0; i < MEJORES.Length; i++)
			{
				MEJOR_MEDIA += (double)MEJORES[i];

			}
			MEJOR_MEDIA = (double)MEJOR_MEDIA / (double)MEJORES.Length;


			for (int i = 0; i < EVS_MEDIA.Length; i++)
			{
				EV_MEDIA += (double)EVS_MEDIA[i];

			}
			EV_MEDIA = (double)EV_MEDIA / (double)EVS_MEDIA.Length;

			for (int i = 0; i < SOLS_ENC.Length; i++)
			{
				SOLUCIONES_MEDIA += (double)SOLS_ENC[i];

			}
			SOLUCIONES_MEDIA = (double)SOLUCIONES_MEDIA / (double)SOLS_ENC.Length;

			
			Console.WriteLine("SALIDAS FINALES");
			Console.WriteLine();
			Console.WriteLine();

			Console.WriteLine("Num generaciones medio:  " + GEN_MEDIA);
			Console.WriteLine("Mejor individuo encontrado: " + MEJOR_MEDIA);
			Console.WriteLine("Numero de evaluaciones: " + (EV_MEDIA * TAMANYO_INDIVIDUO));
			Console.WriteLine("Numero medio de soluciones de fitness 0 o 1 (si el sumatorio es impar) halladas para un maximo establecido de 1: " + SOLUCIONES_MEDIA);

		}

	}
}

