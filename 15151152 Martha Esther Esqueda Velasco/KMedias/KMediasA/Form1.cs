﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InputKey;


namespace KMediasA
{
    public partial class Form1 : Form
    {

        List<Punto> PuntosTotales;
        List<Punto> Clusters;
        Punto temporal;
        string datos = "";
        int TodosLosPuntos = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TodosLosPuntos = Convert.ToInt32(numericUpDown1.Value);
            if (TodosLosPuntos != 0)
            {

                Clusters = new List<Punto>();
                Punto Cluster1 = new Punto("Cluster 1", 2, 4);
                Punto Cluster2 = new Punto("Cluster 2", 3, 1);
                Clusters.Add(Cluster1);
                Clusters.Add(Cluster2);
                PuntosTotales = new List<Punto>();
                MessageBox.Show("Agregue los clusters", "Information", MessageBoxButtons.OK);
                for (int s = 0; s <= TodosLosPuntos - 1; s++)
                {
                    double x1, x2, y1, y2;
                    double distanciaMenor=0;
                    string nombre;
                    double dist;
                    nombre = InputDialog.mostrar("Dame el nombre del Punto: " + s);
                    x1 = Convert.ToDouble(InputDialog.mostrar("Dame valor en X"));
                    y1 = Convert.ToDouble(InputDialog.mostrar("Dame valor en Y"));
                    for (int x = 0; x <= Clusters.Count - 1; x++)
                    {
                        x2 = Clusters.ElementAt(x).DevolverX();
                        y2 = Clusters.ElementAt(x).DevolverY();
                        dist = ObtenerDistancia(x1, y1, x2, y2);
                        temporal = new Punto(nombre, x1, y1, dist, Clusters.ElementAt(x).DevolverNombre());
                        if(distanciaMenor == 0)
                        {
                            PuntosTotales.Add(temporal);
                            distanciaMenor = dist;
                           
                        }
                         else if (dist < distanciaMenor)
                            {
                            PuntosTotales.Add(temporal);
                            PuntosTotales.RemoveAt(x - 1);
                            }                      
                    }
                    distanciaMenor = 0;
                }
                for (int t = 0; t <= PuntosTotales.Count - 1; t++)
                {
                    datos += PuntosTotales.ElementAt(t).GetDatos();
                }
                richTextBox1.Text = datos;
                datos = "";
            }
            else
            {
                MessageBox.Show("Agregue una cantidad valida");
            }


        }

        public double ObtenerDistancia(double x1, double y1, double x2, double y2)
        {
            double Valor;
            Valor = Math.Sqrt(Math.Pow((x1-x2),2) + Math.Pow((y1-y2),2));
            return Valor;
        }
     

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //richTextBox1.Text = Convert.ToString(ObtenerDistancia(6, 2));
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
        }
    }
}
