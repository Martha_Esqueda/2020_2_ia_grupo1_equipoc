﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KMediasA
{
    class Punto
    {
        string Nombre;
        double x;
        double y;
        string cluster;
        double distancia;
        public Punto(string nombre,double x, double y, double distancia)
        {
            this.Nombre = nombre;
            this.x = x;
            this.y = y;
            this.distancia = distancia;
        }
        public Punto(string nombre, double x, double y, double distancia, string cluster)
        {
            this.Nombre = nombre;
            this.x = x;
            this.y = y;
            this.distancia = distancia;
            this.cluster = cluster;
        }
        public Punto(string nombre, double x, double y)
        {
            this.Nombre = nombre;
            this.x = x;
            this.y = y;
        }

        public string GetDatos()
        {
            string datos = "";
            datos = "\nNombre del punto: " + Nombre + "\nCoordenadas de: "+Nombre +"("+x+","+y+")\nDistancia de :" +Nombre+" a "+cluster+": " + distancia + "\n";
            return datos;
        }

        public double DevolverX()
        {
            double X = x;
            return X;
        }
        public double DevolverY()
        {
            double Y = y;
            return Y;
        }
        public string DevolverNombre()
        {
            string nombre;
            nombre = Nombre;
            return nombre;
        }
        public double DevolverDistancia()
        {
            double Distancia;
            Distancia = distancia;
            return Distancia;
        }
    }
}
