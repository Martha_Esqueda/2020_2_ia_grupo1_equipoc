﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Algoritmo_Genetico
{
    public partial class Form1 : Form
    {
        Poblacion LaPoblacion;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void BIiciar_Click(object sender, EventArgs e)
        {
            LaPoblacion = new Poblacion((int) NUDTamaño.Value, new EvaliadorTipo1("12345678") );
            RTBPoblacion.Text = LaPoblacion.GetDatos();
            

        
        }

        private void BCruzar_Click(object sender, EventArgs e)
        {
            LaPoblacion.Cruzamiento();
            RTBCruce.Text = LaPoblacion.GetDatos();


        }

        private void BTSeleccionar_Click(object sender, EventArgs e)
        {
            LaPoblacion.SeleccionSimple((int)NUDPresion.Value);
            RTBSeleccionados.Text = LaPoblacion.GetSeleccion();

        }

        private void bMutar_Click(object sender, EventArgs e)
        {
            LaPoblacion.Mutacion((double)NUDProbabilidad.Value);
            RTBMutacion.Text = LaPoblacion.GetDatos();
            
        }
    }
}
