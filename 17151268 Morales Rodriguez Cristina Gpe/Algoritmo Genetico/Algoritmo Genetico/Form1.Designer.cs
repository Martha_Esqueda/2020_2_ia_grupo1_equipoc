﻿namespace Algoritmo_Genetico
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.NUDTamaño = new System.Windows.Forms.NumericUpDown();
            this.BIiciar = new System.Windows.Forms.Button();
            this.RTBPoblacion = new System.Windows.Forms.RichTextBox();
            this.NUDPresion = new System.Windows.Forms.NumericUpDown();
            this.RTBSeleccionados = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.RTBCruce = new System.Windows.Forms.RichTextBox();
            this.BCruzar = new System.Windows.Forms.Button();
            this.BTSeleccionar = new System.Windows.Forms.Button();
            this.RTBMutacion = new System.Windows.Forms.RichTextBox();
            this.bMutar = new System.Windows.Forms.Button();
            this.NUDProbabilidad = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NUDTamaño)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDPresion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDProbabilidad)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Tamaño de la poblacion";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(272, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Presion";
            // 
            // NUDTamaño
            // 
            this.NUDTamaño.Location = new System.Drawing.Point(160, 17);
            this.NUDTamaño.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.NUDTamaño.Name = "NUDTamaño";
            this.NUDTamaño.Size = new System.Drawing.Size(65, 20);
            this.NUDTamaño.TabIndex = 8;
            this.NUDTamaño.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // BIiciar
            // 
            this.BIiciar.Location = new System.Drawing.Point(239, 14);
            this.BIiciar.Name = "BIiciar";
            this.BIiciar.Size = new System.Drawing.Size(75, 23);
            this.BIiciar.TabIndex = 12;
            this.BIiciar.Text = "Iniciar";
            this.BIiciar.UseVisualStyleBackColor = true;
            this.BIiciar.Click += new System.EventHandler(this.BIiciar_Click);
            // 
            // RTBPoblacion
            // 
            this.RTBPoblacion.Location = new System.Drawing.Point(36, 43);
            this.RTBPoblacion.Name = "RTBPoblacion";
            this.RTBPoblacion.ReadOnly = true;
            this.RTBPoblacion.Size = new System.Drawing.Size(228, 305);
            this.RTBPoblacion.TabIndex = 13;
            this.RTBPoblacion.Text = "";
            // 
            // NUDPresion
            // 
            this.NUDPresion.Location = new System.Drawing.Point(320, 51);
            this.NUDPresion.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.NUDPresion.Name = "NUDPresion";
            this.NUDPresion.Size = new System.Drawing.Size(65, 20);
            this.NUDPresion.TabIndex = 9;
            this.NUDPresion.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // RTBSeleccionados
            // 
            this.RTBSeleccionados.Location = new System.Drawing.Point(275, 80);
            this.RTBSeleccionados.Name = "RTBSeleccionados";
            this.RTBSeleccionados.ReadOnly = true;
            this.RTBSeleccionados.Size = new System.Drawing.Size(163, 268);
            this.RTBSeleccionados.TabIndex = 14;
            this.RTBSeleccionados.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(291, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 15;
            // 
            // RTBCruce
            // 
            this.RTBCruce.Location = new System.Drawing.Point(461, 80);
            this.RTBCruce.Name = "RTBCruce";
            this.RTBCruce.Size = new System.Drawing.Size(170, 268);
            this.RTBCruce.TabIndex = 16;
            this.RTBCruce.Text = "";
            // 
            // BCruzar
            // 
            this.BCruzar.Location = new System.Drawing.Point(556, 51);
            this.BCruzar.Name = "BCruzar";
            this.BCruzar.Size = new System.Drawing.Size(75, 23);
            this.BCruzar.TabIndex = 17;
            this.BCruzar.Text = "Cruzar";
            this.BCruzar.UseVisualStyleBackColor = true;
            this.BCruzar.Click += new System.EventHandler(this.BCruzar_Click);
            // 
            // BTSeleccionar
            // 
            this.BTSeleccionar.Location = new System.Drawing.Point(409, 48);
            this.BTSeleccionar.Name = "BTSeleccionar";
            this.BTSeleccionar.Size = new System.Drawing.Size(75, 23);
            this.BTSeleccionar.TabIndex = 18;
            this.BTSeleccionar.Text = "Seleccionar";
            this.BTSeleccionar.UseVisualStyleBackColor = true;
            this.BTSeleccionar.Click += new System.EventHandler(this.BTSeleccionar_Click);
            // 
            // RTBMutacion
            // 
            this.RTBMutacion.Location = new System.Drawing.Point(651, 50);
            this.RTBMutacion.Name = "RTBMutacion";
            this.RTBMutacion.Size = new System.Drawing.Size(227, 297);
            this.RTBMutacion.TabIndex = 19;
            this.RTBMutacion.Text = "";
            // 
            // bMutar
            // 
            this.bMutar.Location = new System.Drawing.Point(789, 14);
            this.bMutar.Name = "bMutar";
            this.bMutar.Size = new System.Drawing.Size(75, 23);
            this.bMutar.TabIndex = 22;
            this.bMutar.Text = "Mutar";
            this.bMutar.UseVisualStyleBackColor = true;
            this.bMutar.Click += new System.EventHandler(this.bMutar_Click);
            // 
            // NUDProbabilidad
            // 
            this.NUDProbabilidad.DecimalPlaces = 2;
            this.NUDProbabilidad.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.NUDProbabilidad.Location = new System.Drawing.Point(710, 17);
            this.NUDProbabilidad.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.NUDProbabilidad.Name = "NUDProbabilidad";
            this.NUDProbabilidad.Size = new System.Drawing.Size(65, 20);
            this.NUDProbabilidad.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(577, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Probabilidad de Mutacion";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 371);
            this.Controls.Add(this.bMutar);
            this.Controls.Add(this.NUDProbabilidad);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.RTBMutacion);
            this.Controls.Add(this.BTSeleccionar);
            this.Controls.Add(this.BCruzar);
            this.Controls.Add(this.RTBCruce);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RTBSeleccionados);
            this.Controls.Add(this.RTBPoblacion);
            this.Controls.Add(this.BIiciar);
            this.Controls.Add(this.NUDPresion);
            this.Controls.Add(this.NUDTamaño);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.MinimumSize = new System.Drawing.Size(472, 410);
            this.Name = "Form1";
            this.Text = "Algoritmo Genetico";
            ((System.ComponentModel.ISupportInitialize)(this.NUDTamaño)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDPresion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDProbabilidad)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown NUDTamaño;
        private System.Windows.Forms.Button BIiciar;
        private System.Windows.Forms.RichTextBox RTBPoblacion;
        private System.Windows.Forms.NumericUpDown NUDPresion;
        private System.Windows.Forms.RichTextBox RTBSeleccionados;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox RTBCruce;
        private System.Windows.Forms.Button BCruzar;
        private System.Windows.Forms.Button BTSeleccionar;
        private System.Windows.Forms.RichTextBox RTBMutacion;
        private System.Windows.Forms.Button bMutar;
        private System.Windows.Forms.NumericUpDown NUDProbabilidad;
        private System.Windows.Forms.Label label2;
    }
}

