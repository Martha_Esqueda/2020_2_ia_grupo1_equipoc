﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algoritmo_Genetico
{
    class Poblacion
    {
        List<Individuo> Individuos;
        List<Individuo> IndividuosSeleccionados;
        Random GeneradorAleatorio;
        Evaluador ElEvaluador;

        public Poblacion(int TamaPoblacion, Evaluador ElEvaluador)
        {
            Individuos = new List<Individuo>();
            GeneradorAleatorio = new Random();
            this.ElEvaluador = ElEvaluador;

            for (int i = 0; i < TamaPoblacion; i++)
                Individuos.Add(new Individuo(ElEvaluador, GeneradorAleatorio));                                 
        }

        public void SeleccionSimple(int Presion)
        {
            IndividuosSeleccionados = new List<Individuo>();
            int cantidad_Seleccionados = (int)Math.Round((100.0 - Presion)) * Individuos.Count / 100;
           // for (int i = 0; i < cantidad_Seleccionados; i++)
             //   IndividuosSeleccionados.Add(Individuos[i]);

           for (int x = 9; x > 0; x--)
              for (int i = 0; i < Individuos.Count; i++)                
                if (IndividuosSeleccionados.Count < cantidad_Seleccionados)                    
                  if (Individuos[i].GetFitness() == x)                        
                    IndividuosSeleccionados.Add(new Individuo(Individuos[i].GetGenotipo(), Individuos[i].GetFitness()));

        }
        public void OrdenarPorFitness()
        {
            Individuo individuo_auxiliar = null;
            for (int i = 0; i < Individuos.Count-1; i++)
                for (int j = i+1; j < Individuos.Count; j++)
                    if (Individuos[j].GetFitness() > Individuos[i].GetFitness())
                    {
                        individuo_auxiliar = Individuos[i];
                        Individuos[i] = Individuos[j];
                        Individuos[j] = individuo_auxiliar;

                    }
                       
        }
        public string GetDatos()
        {
            StringBuilder datos = new StringBuilder();
            for (int i = 0; i < Individuos.Count; i++)

                datos.Append(Individuos[i].GetDatos() + "\n");

            return datos.ToString();

        }

        public string GetSeleccion()
        {
            StringBuilder datos = new StringBuilder();

            for (int i = 0; i < IndividuosSeleccionados.Count; i++)
                datos.Append(IndividuosSeleccionados[i].GetDatos() + "\n");

            return datos.ToString();
        }

        public void Cruzamiento()
        {
            int indice_Padre;
            int indice_Madre;
            Individuo Hijo;

            int cantidad_Hijos= Individuos.Count-IndividuosSeleccionados.Count;

            for (int i=0; i < cantidad_Hijos; i++)
            {
                indice_Padre = GeneradorAleatorio.Next(IndividuosSeleccionados.Count);
                indice_Madre = GeneradorAleatorio.Next(IndividuosSeleccionados.Count);

                Hijo = new Individuo(ElEvaluador, GeneradorAleatorio, IndividuosSeleccionados[indice_Padre], IndividuosSeleccionados[indice_Madre]);
                Individuos[IndividuosSeleccionados.Count + i]=Hijo;


            }
           OrdenarPorFitness();





        }
        public void Mutacion(double porcentaje_mutacion)
        {
            for (int i = 0; i < Individuos.Count; i++)
                Individuos[i].Mutar(porcentaje_mutacion);

            OrdenarPorFitness();
        }

    }
   
}
