﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algoritmo_Genetico
{
    class Individuo
    {
        string Genotipo;
        int Fitness;

        Evaluador ElEvaluador;
        Random GeneradorAleatorio;

        public Individuo(Evaluador ElEvaluador, Random GeneradorAleatorio)
        {
            this.ElEvaluador = ElEvaluador;
            this.GeneradorAleatorio = GeneradorAleatorio;
            Genotipo = "";
            Fitness = 0;


            for (int i = 0; i < ElEvaluador.GetModelo().Length; i++)
                Genotipo += GeneradorAleatorio.Next(0, 9);

            Fitness = ElEvaluador.Evaluar(Genotipo);

        }
        public Individuo(string Genotipo, int Fitness)
        {
            this.Genotipo = Genotipo;
            this.Fitness = Fitness;
        }

        public Individuo(Evaluador ElEvaluador, Random GeneradorAleatorio, Individuo Padre, Individuo Madre)
        {
            this.ElEvaluador = ElEvaluador;
            this.GeneradorAleatorio = GeneradorAleatorio;
            Fitness = 0;
            Genotipo = "";
            int punto_De_Cruce = GeneradorAleatorio.Next(ElEvaluador.GetModelo().Length);

            Genotipo = Padre.Genotipo.Substring(0, punto_De_Cruce);
            Genotipo += Madre.Genotipo.Substring(punto_De_Cruce);
            Fitness = ElEvaluador.Evaluar(Genotipo);
        }

        public void Mutar(double porcentaje_mutacion)
        {
            double probabilidadMutacion = porcentaje_mutacion / 100;
            string GenotipoMutado = "";

            for (int i = 0; i < Genotipo.Length; i++)
                if (GeneradorAleatorio.NextDouble() < probabilidadMutacion)
                {
                    GenotipoMutado += GeneradorAleatorio.Next(0, 9);

                }
                else
                {
                    GenotipoMutado += Genotipo[i];

                }
            Genotipo = GenotipoMutado;
            Fitness = ElEvaluador.Evaluar(Genotipo);

        }


        public int GetFitness()
        {
            return Fitness;
        }

        public string GetGenotipo()
        {
            return Genotipo;
        }
        public string GetDatos()
        {
            return Genotipo + "-" + Fitness;
        }

    }

   
}
