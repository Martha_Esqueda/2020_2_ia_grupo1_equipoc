﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algoritmo_Genetico
{
    class Evaluador
    {
        public virtual int Evaluar(string Genotipo)
        {
            return 0;

        }
        public virtual string GetModelo()
        {
            return "";

        }

    }
    class EvaliadorTipo1 : Evaluador
    {
        string Modelo;//
        
        public EvaliadorTipo1 (string Modelo)
        {
            this.Modelo = Modelo;
        }

        public override int Evaluar(string Genotipo)
        {
            int fitness = 0;

            for (int i = 0; i < Genotipo.Length; i++)
                if (Genotipo[i] == Modelo[i])
                    fitness++;


            return fitness;

        }

        public override string GetModelo()
        {
            return Modelo;

        }
    }
}
