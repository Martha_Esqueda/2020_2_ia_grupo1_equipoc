﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjerciciosPOO
{
    class Alumno
    {
        string NumControl;
        string Nombre;
        string Carrera;
        string Escuela;

        int Semestre;
        int Promedio;
        int Creditos;

        bool Sexo;
        int Edad;

        List< Materia> Materias;

        public Alumno(string NumControl, string Nombre)
        {
            this.NumControl = NumControl;
            this.Nombre = Nombre;
            Semestre = 1;
            Promedio = 0;
            Creditos = 0;
            Materias = new List<Materia>();



        }
        public int CalcularPromedio()
        {
            int acumuladorCalificaciones = 0;
            for (int i = 0; i < Materias.Count; i++)
            
                acumuladorCalificaciones += Materias[i].GetCalificacion();
                Promedio = acumuladorCalificaciones / Materias.Count;
                                            
            return Promedio;          

        }
        public void AgregarMateria(Materia MateriaNueva)
        {
            Materias.Add(MateriaNueva);

        }
        //Metodo para que se valide sola la materia 
        public string CursarMateria(string ClaveMateria, int Calificacion)
        {
            string error = "";

            for (int i=0; i < Materias.Count; i++)
            {
                error = Materias[i].CursarMateria(ClaveMateria, Calificacion);
                if (error == "")
                   return "";
                if (error != "No es la misma materia")
                    return error;
                
            }
            return "El alumno no esta inscrito a La materia ";
        }

        public int ContabilizarCreditos()
        {
            Creditos = 0;

            for (int i = 0; i < Materias.Count; i++)
                Creditos += Materias[i].GetCreditos();

            return Creditos;
        }
        public string GetDatosMaterias()
        {
            StringBuilder datos = new StringBuilder();
            for (int i = 0; i < Materias.Count; i++)
                datos.Append(Materias[i].GetDatosMateria()+"\n");

                return datos.ToString();
        }
    }
}
