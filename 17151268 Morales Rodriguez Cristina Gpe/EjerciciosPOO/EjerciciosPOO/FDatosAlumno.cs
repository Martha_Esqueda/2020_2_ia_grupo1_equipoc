﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjerciciosPOO
{
    public partial class FDatosAlumno : Form
    {
        Alumno ElAlumno= null;

        public FDatosAlumno()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void BIA_Click(object sender, EventArgs e)
        {
            ElAlumno = new Alumno(TBClaveAlumno.Text, TBNombeAlumno.Text);
            BAgregarMateria.Enabled = true;

        }

        private void BAgregarMateria_Click(object sender, EventArgs e)
        {
            FDatosMateria ventanaMateria = new FDatosMateria();
            if (ventanaMateria.ShowDialog()== DialogResult.OK)
            {
                ElAlumno.AgregarMateria(ventanaMateria.GetMateria());
            
                RTBMaterias.Text = ElAlumno.GetDatosMaterias();

            }
            
            

        }

        private void BCursarMateria_Click(object sender, EventArgs e)
        {
            string error = ElAlumno.CursarMateria(tBClaveMateriaCursar.Text, (int)NUDCalificacionMateriaCursar.Value);
            if (error != "" )
                MessageBox.Show("No fue posible cursar la materia");
            else
                MessageBox.Show("Materia Cursada Exitosamente");

        }
    }
}
