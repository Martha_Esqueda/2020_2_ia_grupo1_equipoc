﻿namespace EjerciciosPOO
{
    partial class FDatosAlumno
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TBNombeAlumno = new System.Windows.Forms.TextBox();
            this.TBClaveAlumno = new System.Windows.Forms.TextBox();
            this.BIA = new System.Windows.Forms.Button();
            this.BAgregarMateria = new System.Windows.Forms.Button();
            this.RTBMaterias = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tBClaveMateriaCursar = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.NUDCalificacionMateriaCursar = new System.Windows.Forms.NumericUpDown();
            this.BCursarMateria = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NUDCalificacionMateriaCursar)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre del Alumno";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Clave del Alumno";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // TBNombeAlumno
            // 
            this.TBNombeAlumno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBNombeAlumno.Location = new System.Drawing.Point(130, 19);
            this.TBNombeAlumno.Name = "TBNombeAlumno";
            this.TBNombeAlumno.Size = new System.Drawing.Size(188, 20);
            this.TBNombeAlumno.TabIndex = 2;
            // 
            // TBClaveAlumno
            // 
            this.TBClaveAlumno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBClaveAlumno.Location = new System.Drawing.Point(130, 52);
            this.TBClaveAlumno.Name = "TBClaveAlumno";
            this.TBClaveAlumno.Size = new System.Drawing.Size(188, 20);
            this.TBClaveAlumno.TabIndex = 3;
            // 
            // BIA
            // 
            this.BIA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BIA.Location = new System.Drawing.Point(193, 114);
            this.BIA.Name = "BIA";
            this.BIA.Size = new System.Drawing.Size(125, 23);
            this.BIA.TabIndex = 4;
            this.BIA.Text = "InscribirAlumno";
            this.BIA.UseVisualStyleBackColor = true;
            this.BIA.Click += new System.EventHandler(this.BIA_Click);
            // 
            // BAgregarMateria
            // 
            this.BAgregarMateria.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BAgregarMateria.Enabled = false;
            this.BAgregarMateria.Location = new System.Drawing.Point(193, 154);
            this.BAgregarMateria.Name = "BAgregarMateria";
            this.BAgregarMateria.Size = new System.Drawing.Size(125, 22);
            this.BAgregarMateria.TabIndex = 5;
            this.BAgregarMateria.Text = "Agregar Materia";
            this.BAgregarMateria.UseVisualStyleBackColor = true;
            this.BAgregarMateria.Click += new System.EventHandler(this.BAgregarMateria_Click);
            // 
            // RTBMaterias
            // 
            this.RTBMaterias.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RTBMaterias.Location = new System.Drawing.Point(28, 186);
            this.RTBMaterias.Name = "RTBMaterias";
            this.RTBMaterias.ReadOnly = true;
            this.RTBMaterias.Size = new System.Drawing.Size(290, 145);
            this.RTBMaterias.TabIndex = 6;
            this.RTBMaterias.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Materias";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 346);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Clave de la Materia a Cursar";
            // 
            // tBClaveMateriaCursar
            // 
            this.tBClaveMateriaCursar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tBClaveMateriaCursar.Location = new System.Drawing.Point(206, 346);
            this.tBClaveMateriaCursar.Name = "tBClaveMateriaCursar";
            this.tBClaveMateriaCursar.Size = new System.Drawing.Size(111, 20);
            this.tBClaveMateriaCursar.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 381);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(167, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Calificacion de la Materia a Cursar";
            // 
            // NUDCalificacionMateriaCursar
            // 
            this.NUDCalificacionMateriaCursar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NUDCalificacionMateriaCursar.Location = new System.Drawing.Point(206, 381);
            this.NUDCalificacionMateriaCursar.Name = "NUDCalificacionMateriaCursar";
            this.NUDCalificacionMateriaCursar.Size = new System.Drawing.Size(111, 20);
            this.NUDCalificacionMateriaCursar.TabIndex = 11;
            // 
            // BCursarMateria
            // 
            this.BCursarMateria.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BCursarMateria.Location = new System.Drawing.Point(206, 415);
            this.BCursarMateria.Name = "BCursarMateria";
            this.BCursarMateria.Size = new System.Drawing.Size(111, 21);
            this.BCursarMateria.TabIndex = 12;
            this.BCursarMateria.Text = "Cursar Materia";
            this.BCursarMateria.UseVisualStyleBackColor = true;
            this.BCursarMateria.Click += new System.EventHandler(this.BCursarMateria_Click);
            // 
            // FDatosAlumno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 450);
            this.Controls.Add(this.BCursarMateria);
            this.Controls.Add(this.NUDCalificacionMateriaCursar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tBClaveMateriaCursar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.RTBMaterias);
            this.Controls.Add(this.BAgregarMateria);
            this.Controls.Add(this.BIA);
            this.Controls.Add(this.TBClaveAlumno);
            this.Controls.Add(this.TBNombeAlumno);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FDatosAlumno";
            this.Text = "Datos Alunmo";
            ((System.ComponentModel.ISupportInitialize)(this.NUDCalificacionMateriaCursar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TBNombeAlumno;
        private System.Windows.Forms.TextBox TBClaveAlumno;
        private System.Windows.Forms.Button BIA;
        private System.Windows.Forms.Button BAgregarMateria;
        private System.Windows.Forms.RichTextBox RTBMaterias;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tBClaveMateriaCursar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown NUDCalificacionMateriaCursar;
        private System.Windows.Forms.Button BCursarMateria;
    }
}

