﻿namespace EjerciciosPOO
{
    partial class FDatosMateria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TBNombreMateria = new System.Windows.Forms.TextBox();
            this.TBClaveMateria = new System.Windows.Forms.TextBox();
            this.NUDCreditos = new System.Windows.Forms.NumericUpDown();
            this.BAceptar = new System.Windows.Forms.Button();
            this.BCamcelar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NUDCreditos)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre de la Materia";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Clave de la Materia";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Creditos";
            // 
            // TBNombreMateria
            // 
            this.TBNombreMateria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBNombreMateria.Location = new System.Drawing.Point(192, 27);
            this.TBNombreMateria.Name = "TBNombreMateria";
            this.TBNombreMateria.Size = new System.Drawing.Size(170, 20);
            this.TBNombreMateria.TabIndex = 3;
            // 
            // TBClaveMateria
            // 
            this.TBClaveMateria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBClaveMateria.Location = new System.Drawing.Point(192, 63);
            this.TBClaveMateria.Name = "TBClaveMateria";
            this.TBClaveMateria.Size = new System.Drawing.Size(170, 20);
            this.TBClaveMateria.TabIndex = 5;
            // 
            // NUDCreditos
            // 
            this.NUDCreditos.Location = new System.Drawing.Point(261, 100);
            this.NUDCreditos.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.NUDCreditos.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.NUDCreditos.Name = "NUDCreditos";
            this.NUDCreditos.Size = new System.Drawing.Size(100, 20);
            this.NUDCreditos.TabIndex = 6;
            this.NUDCreditos.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // BAceptar
            // 
            this.BAceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BAceptar.Location = new System.Drawing.Point(193, 146);
            this.BAceptar.Name = "BAceptar";
            this.BAceptar.Size = new System.Drawing.Size(75, 23);
            this.BAceptar.TabIndex = 7;
            this.BAceptar.Text = "Aceptar";
            this.BAceptar.UseVisualStyleBackColor = true;
            this.BAceptar.Click += new System.EventHandler(this.BAceptar_Click);
            // 
            // BCamcelar
            // 
            this.BCamcelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BCamcelar.Location = new System.Drawing.Point(287, 146);
            this.BCamcelar.Name = "BCamcelar";
            this.BCamcelar.Size = new System.Drawing.Size(75, 23);
            this.BCamcelar.TabIndex = 8;
            this.BCamcelar.Text = "Cancelar";
            this.BCamcelar.UseVisualStyleBackColor = true;
            this.BCamcelar.Click += new System.EventHandler(this.BCamcelar_Click);
            // 
            // FDatosMateria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 213);
            this.Controls.Add(this.BCamcelar);
            this.Controls.Add(this.BAceptar);
            this.Controls.Add(this.NUDCreditos);
            this.Controls.Add(this.TBClaveMateria);
            this.Controls.Add(this.TBNombreMateria);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MinimumSize = new System.Drawing.Size(402, 252);
            this.Name = "FDatosMateria";
            this.Text = "Datos de la Materia";
            ((System.ComponentModel.ISupportInitialize)(this.NUDCreditos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TBNombreMateria;
        private System.Windows.Forms.TextBox TBClaveMateria;
        private System.Windows.Forms.NumericUpDown NUDCreditos;
        private System.Windows.Forms.Button BAceptar;
        private System.Windows.Forms.Button BCamcelar;
    }
}