﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace JMedias
{
    class LectorDatos
    {
        StreamReader Archivo;
        StringBuilder TextoLeido;
        String LineaActual;
        String PathFuente;

        public LectorDatos(String PathFuente)
        {
            if (PathFuente.Length > 0)
            {
                LeerArchivo(PathFuente);
            }
        }

        public void LeerArchivo(String PathFuente)
        {
            this.PathFuente = PathFuente;
            Archivo = new StreamReader(PathFuente);
            TextoLeido = new StringBuilder();
            LineaActual = Archivo.ReadLine();
            while (LineaActual != null)
            {
                TextoLeido.Append(LineaActual);
                TextoLeido.Append("/n");
                LineaActual = Archivo.ReadLine();
            }

        }

        internal string GetTextoLeido()
        {
            return TextoLeido.ToString();

        }
    }
}