﻿namespace KMedias
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OFDTexto = new System.Windows.Forms.OpenFileDialog();
            this.RTBTextoLeido = new System.Windows.Forms.RichTextBox();
            this.registroUno = new System.Windows.Forms.NumericUpDown();
            this.registroDos = new System.Windows.Forms.NumericUpDown();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtResutado = new System.Windows.Forms.TextBox();
            this.btnAleatorio = new System.Windows.Forms.Button();
            this.CentroideX = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCalCen = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.CentroideY = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCentroides = new System.Windows.Forms.TextBox();
            this.tBRCentroide = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.registroUno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.registroDos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CentroideX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CentroideY)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(685, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            this.archivoToolStripMenuItem.Click += new System.EventHandler(this.archivoToolStripMenuItem_Click);
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.abrirToolStripMenuItem.Text = "Abrir";
            this.abrirToolStripMenuItem.Click += new System.EventHandler(this.abrirToolStripMenuItem_Click);
            // 
            // OFDTexto
            // 
            this.OFDTexto.Filter = "Archivos de texto|*.txt|Valores separados por comas|*.csv|Todos los archivos|*.*";
            // 
            // RTBTextoLeido
            // 
            this.RTBTextoLeido.Location = new System.Drawing.Point(11, 25);
            this.RTBTextoLeido.Margin = new System.Windows.Forms.Padding(2);
            this.RTBTextoLeido.Name = "RTBTextoLeido";
            this.RTBTextoLeido.Size = new System.Drawing.Size(642, 197);
            this.RTBTextoLeido.TabIndex = 1;
            this.RTBTextoLeido.Text = "";
            this.RTBTextoLeido.TextChanged += new System.EventHandler(this.RTBTextoLeido_TextChanged);
            // 
            // registroUno
            // 
            this.registroUno.Location = new System.Drawing.Point(55, 267);
            this.registroUno.Margin = new System.Windows.Forms.Padding(2);
            this.registroUno.Name = "registroUno";
            this.registroUno.Size = new System.Drawing.Size(90, 20);
            this.registroUno.TabIndex = 2;
            this.registroUno.ValueChanged += new System.EventHandler(this.registroUno_ValueChanged);
            // 
            // registroDos
            // 
            this.registroDos.Location = new System.Drawing.Point(55, 300);
            this.registroDos.Margin = new System.Windows.Forms.Padding(2);
            this.registroDos.Name = "registroDos";
            this.registroDos.Size = new System.Drawing.Size(90, 20);
            this.registroDos.TabIndex = 3;
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(170, 287);
            this.btnCalcular.Margin = new System.Windows.Forms.Padding(2);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(56, 19);
            this.btnCalcular.TabIndex = 4;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 352);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Distancia=";
            // 
            // txtResutado
            // 
            this.txtResutado.Location = new System.Drawing.Point(115, 349);
            this.txtResutado.Margin = new System.Windows.Forms.Padding(2);
            this.txtResutado.Name = "txtResutado";
            this.txtResutado.ReadOnly = true;
            this.txtResutado.Size = new System.Drawing.Size(76, 20);
            this.txtResutado.TabIndex = 6;
            this.txtResutado.TextChanged += new System.EventHandler(this.txtResutado_TextChanged);
            // 
            // btnAleatorio
            // 
            this.btnAleatorio.Location = new System.Drawing.Point(601, 286);
            this.btnAleatorio.Margin = new System.Windows.Forms.Padding(2);
            this.btnAleatorio.Name = "btnAleatorio";
            this.btnAleatorio.Size = new System.Drawing.Size(76, 19);
            this.btnAleatorio.TabIndex = 17;
            this.btnAleatorio.Text = "Aleatorios";
            this.btnAleatorio.UseVisualStyleBackColor = true;
            this.btnAleatorio.Click += new System.EventHandler(this.btnAleatorio_Click);
            // 
            // CentroideX
            // 
            this.CentroideX.Location = new System.Drawing.Point(396, 287);
            this.CentroideX.Margin = new System.Windows.Forms.Padding(2);
            this.CentroideX.Name = "CentroideX";
            this.CentroideX.Size = new System.Drawing.Size(37, 20);
            this.CentroideX.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(298, 287);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Centroides";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(304, 244);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "CALCULO CENTROIDES";
            // 
            // btnCalCen
            // 
            this.btnCalCen.Location = new System.Drawing.Point(470, 326);
            this.btnCalCen.Margin = new System.Windows.Forms.Padding(2);
            this.btnCalCen.Name = "btnCalCen";
            this.btnCalCen.Size = new System.Drawing.Size(72, 19);
            this.btnCalCen.TabIndex = 19;
            this.btnCalCen.Text = "Calcular";
            this.btnCalCen.UseVisualStyleBackColor = true;
            this.btnCalCen.Click += new System.EventHandler(this.btnCalCen_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(62, 244);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "CALCULO DISTANCIAS";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // CentroideY
            // 
            this.CentroideY.Location = new System.Drawing.Point(456, 286);
            this.CentroideY.Margin = new System.Windows.Forms.Padding(2);
            this.CentroideY.Name = "CentroideY";
            this.CentroideY.Size = new System.Drawing.Size(37, 20);
            this.CentroideY.TabIndex = 21;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(467, 265);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Y";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(406, 265);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "X";
            // 
            // txtCentroides
            // 
            this.txtCentroides.Location = new System.Drawing.Point(530, 285);
            this.txtCentroides.Margin = new System.Windows.Forms.Padding(2);
            this.txtCentroides.Name = "txtCentroides";
            this.txtCentroides.Size = new System.Drawing.Size(67, 20);
            this.txtCentroides.TabIndex = 18;
            // 
            // tBRCentroide
            // 
            this.tBRCentroide.Location = new System.Drawing.Point(466, 352);
            this.tBRCentroide.Margin = new System.Windows.Forms.Padding(2);
            this.tBRCentroide.Name = "tBRCentroide";
            this.tBRCentroide.ReadOnly = true;
            this.tBRCentroide.Size = new System.Drawing.Size(101, 20);
            this.tBRCentroide.TabIndex = 25;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(406, 355);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "Resultado";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 412);
            this.Controls.Add(this.tBRCentroide);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CentroideY);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnCalCen);
            this.Controls.Add(this.txtCentroides);
            this.Controls.Add(this.btnAleatorio);
            this.Controls.Add(this.CentroideX);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtResutado);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.registroDos);
            this.Controls.Add(this.registroUno);
            this.Controls.Add(this.RTBTextoLeido);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.registroUno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.registroDos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CentroideX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CentroideY)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog OFDTexto;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private System.Windows.Forms.RichTextBox RTBTextoLeido;
        private System.Windows.Forms.NumericUpDown registroUno;
        private System.Windows.Forms.NumericUpDown registroDos;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtResutado;
        private System.Windows.Forms.Button btnAleatorio;
        private System.Windows.Forms.NumericUpDown CentroideX;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCalCen;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown CentroideY;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCentroides;
        private System.Windows.Forms.TextBox tBRCentroide;
        private System.Windows.Forms.Label label7;
    }
}

